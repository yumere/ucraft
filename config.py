#coding: utf-8
__author__ = 'yumere'

import sqlite3
import os

## Base Setting ##
baseDIR = os.path.dirname(os.path.abspath(__file__))+"/"


## Database Setting ##
DB_NAME = "ucraft.sql"
db_conn = sqlite3.connect(baseDIR+DB_NAME)
db_cursor = db_conn.cursor()
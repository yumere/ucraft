#coding: utf-8
__author__ = 'yumere'

from flask import Flask, render_template, redirect, url_for, g, request, session
from functools import wraps
from datetime import timedelta

import sqlite3
import argparse

app = Flask(__name__)
app.debug = True
app.secret_key = "2gpan"

def login_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if 'user_id' not in session:
			return redirect(url_for('login'))
		return f(*args, **kwargs)
	return decorated_function

@app.before_request
def before_request():
	g.db_conn = sqlite3.connect("../ucraft.sql")
	g.db_cursor = g.db_conn.cursor()
	session.permanent = True
	app.permanent_session_lifetime = timedelta(minutes=60)

@app.teardown_request
def teardown_request(exception):
	db_cursor = getattr(g, 'db_cursor', None)
	if db_cursor is not None:
		db_cursor.close()

	db_conn = getattr(g, 'db_conn', None)
	if db_conn is not None:
		db_conn.close()

@app.route("/login/", methods=["POST", "GET"])
def login():

	if request.method == "GET":
		return render_template("login.html", title="Ucraft-ToolKit Login")

	elif request.method == "POST":
		try:
			user_id = request.form['user-id']
			user_pw = request.form['user-pw']
		except Exception, e:
			print e
			return redirect(url_for("login"))

		user_count = g.db_cursor.execute("select count(*) from users where user_id=? and user_pw=?", [user_id, user_pw]).fetchone()[0]
		if user_count == 1:
			session['user_id'] = user_id
			return redirect(url_for("index"))
		else:
			return redirect(url_for("login"))

@app.route("/", methods=["GET"])
@login_required
def index():
	return render_template("index.html", title="Ucraft-Toolkit")

@app.route('/user/', methods=["GET"])
@app.route('/user/<string:user_name>', methods=["GET"])
@login_required
def user(user_name=None):
	if user_name == None:
		return "None"

	if request.method == "GET":
		user_list = list()
		db_result = g.db_cursor.execute("select id, user_name, ranking, total_point, ally_id from user_info where user_name like ?", ['%'+user_name+'%']).fetchall()
		for user_id, user_name, ranking, total_point, ally_id in db_result:
			if ally_id is not None:
				ally_id = g.db_cursor.execute("select ally_name from ally_info where id=?", [ally_id]).fetchone()[0]

			planet_result = g.db_cursor.execute("select coordinate, type from planet_info where user_id=?", [user_id]).fetchall()
			planets = list()
			for coordinate, type in planet_result:
				if type == 1:
					planets.append({
						'coordinate': coordinate,
						'type': u"거점행성"
					})
				elif type == 2:
					planets.append({
						'coordinate': coordinate,
						'type': "Home Planet"
					})
				elif type == 3:
					planets.append({
						'coordinate': coordinate,
						'type': "Colony",
					})

			user_list.append(
				{
					'user_name': user_name,
					'ranking': ranking,
					'total_point': total_point,
					'ally_id': ally_id,
					'planets': planets
				}
			)

		return render_template("search_result.html", users=user_list)

	elif request.method == "POST":
		return "POST"

@app.route('/ally/', methods=["GET"])
@app.route('/ally/<string:ally_name>', methods=["GET"])
@login_required
def ally(ally_name=None):
	if ally_name == None:
		return "None"

	return ally_name

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-p', '--port', type=int, help="Input serving port")

	options = parser.parse_args()
	app.run(host="0.0.0.0", port=5000 if options.port is None else options.port)
	pass

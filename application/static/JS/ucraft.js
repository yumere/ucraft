/**
 * Created by yumere on 15. 5. 18..
 */

function search(){
    $("document").ready(function(){
        var type = $("#search-type").val();

        //var galaxy = $("#search-galaxy").val();
        //var system = $("#search-system").val();
        var name = $("#search-name").val();

        console.log(type);
        console.log(name);

        // 유저 검색
        if(type == 0){
            //$("#result-wrapper").load("/user/"+name+"?galaxy="+galaxy+"&system="+system);
            $("#result-wrapper").load('/user/'+name);
            return false;
        }
        // 동맹 검색
        else if(type == 1){
            //$("#result-wrapper").load("/ally/"+name+"?galaxy="+galaxy+"&system="+system);
            $("#result-wrapper").load("/ally/"+name);
            return false;
        }
    });
}

function showPlanet(user_name){
    console.log($("."+user_name).css('display'));
    if($("."+user_name).css('display') == "none"){
        $("."+user_name).css('display', "block");
    }
    else{
        $("."+user_name).css('display', 'none');
    }
}
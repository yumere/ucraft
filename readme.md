# Ucraft Toolkit
## Crawler
Using selenium library, scrap web data from ucraft-online site.
Save the data and serve Flask web application

## Web Application
Using Flask library and Jinja template, serve the Flask web application

## Modules
- Flask (0.10.1)
- itsdangerous (0.24)
- Jinja2 (2.7.3)
- MarkupSafe (0.23)
- selenium (2.45.0)
- Werkzeug (0.10.4)
- wsgiref (0.1.2)
- beautifulsoup4 (4.3.2)
#coding: utf-8
__author__ = 'yumere'

from selenium import webdriver
import bs4
import re
import imp

config = imp.load_source("cofing", "../config.py")

driver = webdriver.Firefox()

def login(user_id, user_pw):
	global driver
	driver.get("http://ucraft-online.com")

	driver.find_element_by_id("UserName").send_keys(user_id)
	elem = driver.find_element_by_id("Password")
	elem.send_keys(user_pw)
	elem.submit()

	driver.find_element_by_class_name("mainpage_game_start_button").click()
	driver.execute_script("on_close_popup();")

def get_all_user():
	global driver

	for i in range(50):
		url = "http://ucraft-online.com/ranking/player/point?page=%d&galaxyIndex=0" % (i+1)
		driver.get(url)
		soup = bs4.BeautifulSoup(driver.page_source)
		table = soup("div", {"class": "grayback"})[3]('tbody')[0]
		trs = table("tr")[1:]

		for tr in trs:
			tds = tr("td")
			ranking = int("".join(tds[0].text.strip().split(",")))
			user_name = str(tds[2].text.strip())
			ally_name = str(tds[3].text.strip())
			total_point = int("".join(tds[4].text.strip().split(",")))

			count, ally_id = config.db_cursor.execute("select count(*), id from ally_info where ally_name like ?", ['%'+ally_name+'%']).fetchone()
			if count != 1:
				try:
					config.db_cursor.execute("insert into user_info(user_name, ranking, total_point) values(?, ?, ?)", [user_name, ranking, total_point])
				except Exception, e:
					config.db_cursor.execute("update user_info set ranking=?, total_point=?, ally_id=? where user_name=?", [ranking, total_point, None, user_name])

			else:
				try:
					config.db_cursor.execute("insert into user_info(user_name, ranking, total_point, ally_id) values(?, ?, ?, ?)", [user_name, ranking, total_point, ally_id])
				except Exception, e:
					config.db_cursor.execute("update user_info set ranking=?, total_point=?, ally_id=? where user_name=?", [ranking, total_point, ally_id, user_name])

		config.db_conn.commit()

def get_all_alliance():
	global driver
	for galaxy in range(4):
		for i in range(5):
			url = "http://ucraft-online.com/ranking/alliance/point?page=%d&galaxyIndex=%d" %(i+1, galaxy+1)
			driver.get(url)

			soup = bs4.BeautifulSoup(driver.page_source)
			table = soup("div", {"class": "grayback"})[3]("tbody")[0]

			trs = table("tr")[1:]
			for tr in trs:
				tds = tr("td")

				ranking = int(tds[0].text.strip())
				ally_name = str(tds[2].text.strip())
				number = int(tds[3].text.strip())
				total_points = int("".join(tds[4].text.strip().split(",")))

				try:
					config.db_cursor.execute("insert into ally_info(ally_name, ranking, number, total_points, galaxy) values(?, ?, ?, ?, ?)", [ally_name, ranking, number, total_points, galaxy+1])
				except Exception, e:
					config.db_cursor.execute("update ally_info set ranking=?, number=?, total_points=?, galaxy=? where ally_name=?", [ranking, number, total_points, galaxy+1, ally_name])

			config.db_conn.commit()

def get_all_planet():
	"""
	types:
	0 = None
	1 = 거점행성
	2 = home planet
	3 = colony
	4 = 자원행성
	"""

	global driver
	Galaxies = [146, 100, 113, 159]

	for galaxy, systems in enumerate(Galaxies):
		for system in range(systems):
			url = "http://ucraft-online.com/galaxy/listview?galaxy=%d&solar=%d&planet=1" % (galaxy+1, system+1)
			driver.get(url)

			soup = bs4.BeautifulSoup(driver.page_source)
			trs = soup("div", {"id": "galaxy_content_panel"})[0]("table")[0]("tr")

			for planet, tr in enumerate(trs[1:-1]):
				td = tr("td")
				try:
					t_galaxy, t_system, t_planet = re.match("(\d+:\d+:\d+)", td[0].text.strip()).group().split(":")
					coordinate = ":".join([t_galaxy, t_system, t_planet])
					# print re.match("([\d]+:[\d]+:[\d]+)", td[0].text.strip()).group()
				except AttributeError:
					continue

				types = td[1].text.strip()
				if bool(re.search("Home Planet", types)):
					types = 2
				elif bool(re.search("Colony", types)):
					types = 3
				elif bool(re.search(u"자원행성", types)):
					types = 4
				elif bool(re.search(u"거점행성", types)):
					types = 1
				else:
					count = config.db_cursor.execute("select count(*) from planet_info where coordinate=?", [coordinate]).fetchone()[0]
					if count == 1:
						config.db_cursor.execute("update planet_info set type=?, user_id=? where coordinate=?", [0, None, coordinate])

					continue

				user_name = re.sub("\([\s\S]+\)", "", td[3].text.strip()).strip()

				user_id, count = config.db_cursor.execute("select id, count(*) from user_info where user_name=?", [user_name]).fetchone()
				if count == 0:
					continue

				try:
					config.db_cursor.execute("insert into planet_info(coordinate, galaxy, system, planet, type, user_id) values(?, ?, ?, ?, ?, ?)", [coordinate, t_galaxy, t_system, t_planet, types, user_id])
				except Exception, e:
					config.db_cursor.execute("update planet_info set type=?, user_id=? where coordinate=?", [types, user_id, coordinate])

			config.db_conn.commit()

if __name__ == "__main__":
	login("yumtest", "qwer1234")

	# get user
	get_all_alliance()
	get_all_user()
	get_all_planet()

	driver.close()

	config.db_cursor.close()
	config.db_conn.close()